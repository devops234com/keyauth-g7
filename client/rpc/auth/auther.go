package auth

import (
	"gitee.com/go-course/keyauth-g7/apps/audit"
	"gitee.com/go-course/keyauth-g7/apps/policy"
	"gitee.com/go-course/keyauth-g7/apps/token"
	"gitee.com/go-course/keyauth-g7/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewKeyauthAuther(client *rpc.ClientSet, serviceName string) *KeyauthAuther {
	return &KeyauthAuther{
		auth:        client.Token(),
		perm:        client.Policy(),
		audit:       client.Audit(),
		log:         zap.L().Named("http.auther"),
		serviceName: serviceName,
	}
}

// 有Keyauth提供的 HTTP认证中间件
//
type KeyauthAuther struct {
	log         logger.Logger
	auth        token.ServiceClient
	perm        policy.RPCClient
	audit       audit.RPCClient
	serviceName string
}
